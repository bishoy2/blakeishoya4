﻿using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Manages a level 3 enemy ship.
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.GameObject" />
    public class EnemyShipLevel3 : EnemyShip
    {
        #region Data members

        private const int SpeedXDirection = 2;
        private const int SpeedYDirection = 0;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShipLevel3" /> class.
        /// </summary>
        public EnemyShipLevel3()
        {
            Sprite = new EnemyShipLevel3Sprite();
            SetSpeed(SpeedXDirection, SpeedYDirection);
            ShipLevel = Level3;
            PointValue = 3;
            CanFire = true;
        }

        #endregion
    }
}