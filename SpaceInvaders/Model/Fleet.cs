﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Models a collection of enemy ships.
    /// </summary>
    public class Fleet
    {
        #region Data Members

        private const int NumberOfLevelOneShips = 2;
        private const int NumberOfLevelTwoShips = 4;
        private const int NumberOfLevelThreeShips = 6;
        private const int NumberOfLevelFourShips = 8;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the enemy ships.
        /// </summary>
        /// <value>
        /// The enemy ships.
        /// </value>
        public List<EnemyShip> EnemyShips { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Creates the fleet.
        /// </summary>
        public void CreateFleet()
        {
            this.EnemyShips = new List<EnemyShip>();
            for (var i = 0; i < NumberOfLevelOneShips; i++)
            {
                this.EnemyShips.Add(new EnemyShipLevel1());
            }
            for (var i = 0; i < NumberOfLevelTwoShips; i++)
            {
                this.EnemyShips.Add(new EnemyShipLevel2());
            }
            for (var i = 0; i < NumberOfLevelThreeShips; i++)
            {
                this.EnemyShips.Add(new EnemyShipLevel3());
            }
            for (var i = 0; i < NumberOfLevelFourShips; i++)
            {
                this.EnemyShips.Add(new EnemyShipLevel4());
            }

        }

        /// <summary>
        ///     Moves the enemy ships left.
        ///     Precondition: None
        ///     Postcondition: Enemy Ships will all move to the left.
        /// </summary>
        public void MoveFleetLeft()
        {
            foreach (var enemyShip in this.EnemyShips)
            {
                enemyShip.MoveLeft();
            }
        }

        /// <summary>
        ///     Moves the enemy ships right.
        ///     Precondition: None
        ///     Postcondition: Enemy Ships will all move to the right.
        /// </summary>
        public void MoveFleetRight()
        {
            foreach (var enemyShip in this.EnemyShips)
            {
                enemyShip.MoveRight();
            }
        }

        /// <summary>
        ///     Checks to see if the fleet is destroyed.
        ///     Precondition: none
        /// </summary>
        /// <returns>True if the fleet collection is empty, false otherwise.</returns>
        public bool FleetDestroyed()
        {
            return this.EnemyShips.Count == 0;
        }

        #endregion
    }
}
