﻿using System;
using System.Collections.Generic;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Manages the entire game.
    /// </summary>
    public class GameManager
    {
        #region Data members

        private const double PlayerShipBottomOffset = 30;
        private const double SpaceBetweenEnemyShips = 20;

        private const int EnemyHitRange = 300;
        private const int EnemyHitChance = 20;
        private const int EnemyMinHitRange = 0;

        private Canvas background;

        private readonly double backgroundHeight;
        private readonly double backgroundWidth;

        private PlayerShip playerShip;
        private Fleet enemyFleet;

        private double firstRowOfEnemyShipsY;
        private double secondRowOfEnemyShipsY;
        private double thirdRowOfEnemyShipsY;
        private double fourthRowOfEnemyShipsY;
        private double initialEnemyShipOffsetX;

        private List<Bullet> activeEnemyBullets;
        private List<Bullet> activePlayerBullets;
        private Random randomEnemyFire;
       
        private const int MaxEnemySteps = 62;
        private const int MidEnemySteps = 31;

        private DispatcherTimer timer;
        private readonly TimeSpan gameTickInterval = new TimeSpan(0, 0, 0, 0, TickInterval);
        private int enemyShipTickCount;

        #endregion

        #region Properties
        
        /// <summary>
        ///     The tick interval in milliseconds.
        /// </summary>
        public const int TickInterval = 1;

        /// <summary>
        ///     Gets or sets the player's score.
        /// </summary>
        /// <value>
        ///     The player's score.
        /// </value>
        public int Score { get; private set; }

        /// <summary>
        ///     Gets or sets the player's lives.
        /// </summary>
        /// <value>
        ///     The player's lives
        /// </value>
        public int PlayerLives { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="GameManager" /> class.
        ///     Precondition: backgroundHeight > 0 AND backgroundWidth > 0
        /// </summary>
        /// <param name="backgroundHeight">The backgroundHeight of the game play window.</param>
        /// <param name="backgroundWidth">The backgroundWidth of the game play window.</param>
        public GameManager(double backgroundHeight, double backgroundWidth)
        {
            if (backgroundHeight <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(backgroundHeight));
            }

            if (backgroundWidth <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(backgroundWidth));
            }

            this.backgroundHeight = backgroundHeight;
            this.backgroundWidth = backgroundWidth;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Initializes the game placing player ship and enemy ship in the game.
        ///     Precondition: background != null
        ///     Postcondition: Game is initialized and ready for play.
        /// </summary>
        /// <param name="backgroundCanvas">The background canvas.</param>
        public void InitializeGame(Canvas backgroundCanvas)
        {
            if (backgroundCanvas == null)
            {
                throw new ArgumentNullException(nameof(backgroundCanvas));
            }

            this.background = backgroundCanvas;

            this.createPlayerShip();
            this.placePlayerShipNearBottomOfBackgroundCentered();

            this.enemyFleet = new Fleet();
            this.createEnemyShips();
            this.firstRowOfEnemyShipsY = 4 * this.enemyFleet.EnemyShips[0].Height + SpaceBetweenEnemyShips * 3;
            this.secondRowOfEnemyShipsY = 3*this.enemyFleet.EnemyShips[0].Height + SpaceBetweenEnemyShips*2;
            this.thirdRowOfEnemyShipsY =  2*this.enemyFleet.EnemyShips[0].Height + SpaceBetweenEnemyShips;
            this.fourthRowOfEnemyShipsY = this.enemyFleet.EnemyShips[0].Height;
            this.initialEnemyShipOffsetX = (this.backgroundWidth - this.enemyFleet.EnemyShips.Count*this.enemyFleet.EnemyShips[0].Width/3 -
                                            SpaceBetweenEnemyShips*this.enemyFleet.EnemyShips.Count/3)/2;
            this.placeEnemyShips();
            this.activeEnemyBullets = new List<Bullet>();
            this.activePlayerBullets = new List<Bullet>();
            this.randomEnemyFire = new Random();

            this.enemyShipTickCount = 0;
            this.PlayerLives = 3;

            this.timer = new DispatcherTimer { Interval = this.gameTickInterval };
            this.timer.Tick += this.handleNormalGameplay;
            this.timer.Start();
        }

        private void createEnemyShips()
        {
            this.enemyFleet.CreateFleet();
            foreach (var enemyShip in this.enemyFleet.EnemyShips)
            {
                this.background.Children.Add(enemyShip.Sprite);
            }
        }

        private void placeEnemyShips()
        {
            var level4ShipXOffset = this.initialEnemyShipOffsetX;
            var level3ShipXOffset = this.initialEnemyShipOffsetX + this.enemyFleet.EnemyShips[0].Width + SpaceBetweenEnemyShips;
            var level2ShipXOffset = this.initialEnemyShipOffsetX + 2*(this.enemyFleet.EnemyShips[0].Width + SpaceBetweenEnemyShips);
            var level1ShipXOffset = this.initialEnemyShipOffsetX + 3*(this.enemyFleet.EnemyShips[0].Width + SpaceBetweenEnemyShips);
            foreach (var enemyShip in this.enemyFleet.EnemyShips)
            {
                switch (enemyShip.ShipLevel)
                {
                    case 4:
                        level4ShipXOffset = this.setLevel4EnemyShipCoordinates(enemyShip, level4ShipXOffset);
                        break;
                    case 3:
                        level3ShipXOffset = this.setLevel3EnemyShipCoordinates(enemyShip, level3ShipXOffset);
                        break;
                    case 2:
                        level2ShipXOffset = this.setLevel2EnemyShipCoordinates(enemyShip, level2ShipXOffset);
                        break;
                    case 1:
                        level1ShipXOffset = this.setLevel1EnemyShipCoordinates(enemyShip, level1ShipXOffset);
                        break;
                    default:
                        throw new Exception("Invalid enemy ship level detected.");
                }
            }
        }

        private double setLevel1EnemyShipCoordinates(EnemyShip enemyShip, double level1ShipXOffset)
        {
            enemyShip.Y = this.firstRowOfEnemyShipsY;
            enemyShip.X = SpaceBetweenEnemyShips + level1ShipXOffset;
            level1ShipXOffset += enemyShip.Width + SpaceBetweenEnemyShips;
            return level1ShipXOffset;
        }

        private double setLevel2EnemyShipCoordinates(EnemyShip enemyShip, double level2ShipXOffset)
        {
            enemyShip.Y = this.secondRowOfEnemyShipsY;
            enemyShip.X = SpaceBetweenEnemyShips + level2ShipXOffset;
            level2ShipXOffset += enemyShip.Width + SpaceBetweenEnemyShips;
            return level2ShipXOffset;
        }

        private double setLevel3EnemyShipCoordinates(EnemyShip enemyShip, double level3ShipXOffset)
        {
            enemyShip.Y = this.thirdRowOfEnemyShipsY;
            enemyShip.X = SpaceBetweenEnemyShips + level3ShipXOffset;
            level3ShipXOffset += enemyShip.Width + SpaceBetweenEnemyShips;
            return level3ShipXOffset;
        }
        private double setLevel4EnemyShipCoordinates(EnemyShip enemyShip, double level4ShipXOffset)
        {
            enemyShip.Y = this.fourthRowOfEnemyShipsY;
            enemyShip.X = SpaceBetweenEnemyShips + level4ShipXOffset;
            level4ShipXOffset += enemyShip.Width + SpaceBetweenEnemyShips;
            return level4ShipXOffset;
        }

        private void createPlayerShip()
        {
            this.playerShip = new PlayerShip();
            this.background.Children.Add(this.playerShip.Sprite);
        }

        private void placePlayerShipNearBottomOfBackgroundCentered()
        {
            this.playerShip.X = this.backgroundWidth/2 - this.playerShip.Width/2.0;
            this.playerShip.Y = this.backgroundHeight - this.playerShip.Height - PlayerShipBottomOffset;
        }

        /// <summary>
        ///     Moves the player ship to the left.
        ///     Precondition: player ship must not be at the left boundary of the page.
        ///     Postcondition: The player ship has moved left.
        /// </summary>
        public void MovePlayerShipLeft()
        {
            if (!this.playerShipIsAtLeftCanvasBoundary())
            {
                this.playerShip.MoveLeft();
            }
        }

        /// <summary>
        ///     Moves the player ship to the right.
        ///     Precondition: player ship must not be at the right boundary of the page.
        ///     Postcondition: The player ship has moved right.
        /// </summary>
        public void MovePlayerShipRight()
        {
            if (!this.playerShipIsAtRightCanvasBoundary())
            {
                this.playerShip.MoveRight();
            }
        }

        private bool playerShipIsAtLeftCanvasBoundary()
        {
            return this.playerShip.X <= this.playerShip.SpeedX;
        }

        private bool playerShipIsAtRightCanvasBoundary()
        {
            return this.playerShip.X + this.playerShip.Width >= this.backgroundWidth - this.playerShip.SpeedX;
        }

        /// <summary>
        ///     Fires the player bullet.
        ///     Precondition: Player cannot have another bullet active.
        ///     Postcondition: The PlayerShip fires a bullet.
        /// </summary>
        public void FirePlayerBullet()
        {
            if (this.playerHasLessThanOrEqualTo3ActiveBullets())
            {
                var newPlayerBullet = new Bullet {IsActive = true};
                this.activePlayerBullets.Add(newPlayerBullet);
                this.background.Children.Add(newPlayerBullet.Sprite);
                newPlayerBullet.Y = this.backgroundHeight - this.playerShip.Height - PlayerShipBottomOffset;
                newPlayerBullet.X = this.playerShip.X + this.playerShip.Width/2;
            }
        }

        private void movePlayerBullets()
        {
            foreach (var bullet in this.activePlayerBullets)
            {
                if (bullet.IsActive)
                {
                    bullet.MoveUp();
                }
                this.checkEnemiesForCollisionsWithPlayerBullet(bullet);
                if (!this.activePlayerBullets.Contains(bullet))
                {
                    break;
                }
            }            
        }

        private void checkEnemiesForCollisionsWithPlayerBullet(Bullet playerBullet)
        {
            foreach (var enemyShip in this.enemyFleet.EnemyShips)
            {
                if (this.findCollisionBetween(enemyShip, playerBullet))
                {
                    this.removeBulletFromPlay(playerBullet);
                    this.destroyEnemyShip(enemyShip);
                    this.updatePlayerScore(enemyShip.PointValue);
                    break;
                }
            }
        }

        private void updatePlayerScore(int enemyShipPointValue)
        {
            this.Score += enemyShipPointValue;
        }

        private void destroyEnemyShip(EnemyShip enemyShip)
        {
            this.background.Children.Remove(enemyShip.Sprite);
            this.enemyFleet.EnemyShips.Remove(enemyShip);
        }


        private void removeBulletFromPlay(Bullet bullet)
        {
            if (this.activePlayerBullets.Contains(bullet))
            {
                this.activePlayerBullets.Remove(bullet);
            }
            else if (this.activeEnemyBullets.Contains(bullet))
            {
                this.activeEnemyBullets.Remove(bullet);
            }
            this.background.Children.Remove(bullet.Sprite);
            bullet.IsActive = false;
        }

        private bool playerHasLessThanOrEqualTo3ActiveBullets()
        {
            var returnValue = this.activePlayerBullets.Count <= 2;
            return returnValue;
        }

        private void removeBulletsPastBoundary()
        {
            foreach (var bullet in this.activePlayerBullets)
            {
                if (bullet.Y < 0)
                {
                    this.activePlayerBullets.Remove(bullet);
                    this.background.Children.Remove(bullet.Sprite);
                    break;
                }
            }
            foreach (var bullet in this.activeEnemyBullets)
            {
                if (bullet.Y > this.backgroundHeight)
                {
                    this.activeEnemyBullets.Remove(bullet);
                    this.background.Children.Remove(bullet.Sprite);
                    break;
                }
            }
        }

        private void handleEnemyFire()
        {
            foreach (var enemyShip in this.enemyFleet.EnemyShips)
            {
                if (this.enemyShipIsReadyToFire(enemyShip))
                {
                    this.fireEnemyBullet(enemyShip);
                }
            }
        }

        private bool enemyShipIsReadyToFire(EnemyShip enemyShip)
        {
            return enemyShip.CanFire && this.randomEnemyFire.Next(EnemyMinHitRange, EnemyHitRange) == EnemyHitChance;
        }

        private void fireEnemyBullet(EnemyShip enemyShip)
        {
            var newEnemyBullet = new Bullet();
            this.activeEnemyBullets.Add(newEnemyBullet);
            this.background.Children.Add(newEnemyBullet.Sprite);
            newEnemyBullet.Y = enemyShip.Y;
            newEnemyBullet.X = enemyShip.X + enemyShip.Width/2;
        }

        private void moveEnemyBullets()
        {
            if (this.activeEnemyBullets.Count > 0)
            {
                foreach (var bullet in this.activeEnemyBullets)
                {
                    if (this.detectCollisionsBetweenEnemyBulletAndPlayer(bullet))
                    {
                        break;
                    }
                }
            }
        }

        private bool detectCollisionsBetweenEnemyBulletAndPlayer(Bullet enemyBullet)
        {
            var collision = false;
            if (this.findCollisionBetween(this.playerShip, enemyBullet) && !this.playerShip.IsDestroyed)
            {
                this.removeBulletFromPlay(enemyBullet);
                this.PlayerLives--;
                collision = true;
                if (this.PlayerLives == 0)
                {
                    this.timer.Stop(); 
                }
            }
            if (enemyBullet.IsActive)
            {
                enemyBullet.MoveDown();
            }
            return collision;
        }

        /// <summary>
        ///     Determines whether the game is over.
        ///     Precondition: None.
        /// </summary>
        /// <returns>
        ///     <c>true</c> if the game is over; otherwise, <c>false</c>.
        /// </returns>
        public bool IsGameOver()
        {
            return this.enemyFleet.FleetDestroyed() || this.PlayerLives == 0;
        }

        private void moveEnemyShipsOnTick()
        {
            this.enemyShipTickCount++;
            if (this.enemyShipTickCount > MidEnemySteps && this.enemyShipTickCount < MaxEnemySteps)
            {
                this.enemyFleet.MoveFleetRight();
            }
            else if (this.enemyShipTickCount <= MidEnemySteps && this.enemyShipTickCount > 0)
            {
                this.enemyFleet.MoveFleetLeft();
            }
            else if (this.enemyShipTickCount == MaxEnemySteps)
            {
                this.enemyShipTickCount = 0;
            }
        }

        private void moveBulletsOnTick()
        {
            this.movePlayerBullets();
            this.moveEnemyBullets();
            this.removeBulletsPastBoundary();
        }

        private void handleNormalGameplay(object sender, object o)
        {
            this.moveEnemyShipsOnTick();
            this.moveBulletsOnTick();
            this.handleEnemyFire();
        }

        private bool findCollisionBetween(GameObject gameObject, Bullet bullet)
        {
            var collision = false;

            if (bullet.IsActive)
            {
                collision = this.calculateCollision(gameObject, false, bullet);
            }

            return collision;
        }

        private bool calculateCollision(GameObject gameObject, bool collision, Bullet bullet)
        {
            Rect gameobjectBox;
            var bulletBox = this.createHitBoxes(gameObject, bullet, out gameobjectBox);
            bulletBox.Intersect(gameobjectBox);

            if (!bulletBox.IsEmpty)
            {
                collision = true;
            }
            return collision;
        }

        private Rect createHitBoxes(GameObject gameObject, Bullet bullet, out Rect gameobjectHitBox)
        {
            var bulletLeft = Canvas.GetLeft(bullet.Sprite);
            var bulletTop = Canvas.GetTop(bullet.Sprite);
            var objectInQuestionLeft = Canvas.GetLeft(gameObject.Sprite);
            var objectInQuestionTop = Canvas.GetTop(gameObject.Sprite);
            var bulletHitBox = new Rect(bulletLeft, bulletTop, bullet.Width, bullet.Height);
            gameobjectHitBox = new Rect(objectInQuestionLeft, objectInQuestionTop, gameObject.Width, gameObject.Height);
            return bulletHitBox;
        }

        #endregion
    }
}