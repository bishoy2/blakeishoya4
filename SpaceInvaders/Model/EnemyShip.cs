﻿namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Defines the basics of an Enemy Ship.
    /// </summary>
    public abstract class EnemyShip : GameObject
    {
        #region Properties

        /// <summary>
        ///     Gets the ship level.
        /// </summary>
        /// <value>
        ///     The ship level.
        /// </value>
        public int ShipLevel { get; protected set; }

        /// <summary>
        ///     Constant for Enemy Ship Level 1.
        /// </summary>
        protected const int Level1 = 1;

        /// <summary>
        ///     Constant for Enemy Ship Level 2.
        /// </summary>
        protected const int Level2 = 2;

        /// <summary>
        ///     Constant for Enemy Ship Level 3.
        /// </summary>
        protected const int Level3 = 3;

        /// <summary>
        ///      Constant for Enemy Ship Level 4.
        /// </summary>
        protected const int Level4 = 4;

        /// <summary>
        /// Gets or sets the point value.
        /// </summary>
        /// <value>
        /// The point value.
        /// </value>
        public int PointValue { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this ship can fire bullets.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance can fire; otherwise, <c>false</c>.
        /// </value>
        public bool CanFire { get; set; }

        #endregion
    }
}