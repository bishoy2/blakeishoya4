﻿using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Manages a level 2 enemy ship.
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.GameObject" />
    public class EnemyShipLevel2 : EnemyShip
    {
        #region Data members

        private const int SpeedXDirection = 2;
        private const int SpeedYDirection = 0;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShipLevel2" /> class.
        /// </summary>
        public EnemyShipLevel2()
        {
            Sprite = new EnemyShipLevel2Sprite();
            SetSpeed(SpeedXDirection, SpeedYDirection);
            ShipLevel = Level2;
            PointValue = 2;
            CanFire = false;
        }

        #endregion
    }
}