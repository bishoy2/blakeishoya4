﻿using System;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using SpaceInvaders.Model;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SpaceInvaders.View
{
    /// <summary>
    ///     An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage
    {
        #region Data members

        private readonly GameManager gameManager;

        private readonly DispatcherTimer timer;
        private readonly TimeSpan gameTickInterval = new TimeSpan(0, 0, 0, 0, TickInterval);

        #endregion

        #region Properties

        /// <summary>
        ///     The tick interval in milliseconds.
        /// </summary>
        public const int TickInterval = 1;

        /// <summary>
        ///     The application height
        /// </summary>
        public const double ApplicationHeight = 480;

        /// <summary>
        ///     The application width
        /// </summary>
        public const double ApplicationWidth = 640;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="MainPage" /> class.
        /// </summary>
        public MainPage()
        {
            this.InitializeComponent();

            ApplicationView.PreferredLaunchViewSize = new Size {Width = ApplicationWidth, Height = ApplicationHeight};
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;
            ApplicationView.GetForCurrentView().SetPreferredMinSize(new Size(ApplicationWidth, ApplicationHeight));

            this.gameManager = new GameManager(ApplicationHeight, ApplicationWidth);

            Window.Current.CoreWindow.KeyDown += this.coreWindowOnKeyDown;

            this.gameManager.InitializeGame(this.theCanvas);

            this.timer = new DispatcherTimer { Interval = this.gameTickInterval };
            this.timer.Tick += this.timerOnTick;
            this.timer.Start();
        }

        #endregion

        #region Methods

        private async void timerOnTick(object sender, object o)
        {
            if (this.gameManager.IsGameOver())
            {
                this.timer.Stop();
                await this.showGameOverDialog();
            }

            this.scoreTextBlock.Text = "Score: " + this.gameManager.Score;
            this.livesTextBlock.Text = "Lives: " + this.gameManager.PlayerLives;
        }

        private async Task showGameOverDialog()
        {
            var gameOverMessage = "";
            if (this.gameManager.PlayerLives == 0)
            {
                gameOverMessage = "YOU LOSE.";
            }
            else
            {
                gameOverMessage = "YOU WIN!";
            }
            var gameOverDialog = new MessageDialog(gameOverMessage);
            gameOverDialog.Commands.Add(new UICommand("Exit", closeGame));
            await gameOverDialog.ShowAsync();
        }

        private static void closeGame(IUICommand command)
        {
            CoreApplication.Exit();
        }

        private void coreWindowOnKeyDown(CoreWindow sender, KeyEventArgs args)
        {
            switch (args.VirtualKey)
            {
                case VirtualKey.Left:
                    this.gameManager.MovePlayerShipLeft();
                    break;
                case VirtualKey.Right:
                    this.gameManager.MovePlayerShipRight();
                    break;
                case VirtualKey.Space:
                    this.gameManager.FirePlayerBullet();
                    break;
            }
        }

        #endregion
    }
}